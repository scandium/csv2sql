# -*- coding: utf-8 -*-
'''
Created on 16.06.2014
@author: Egor N. Kostyk
'''

from OpenCartExtra import CSVDataManipulation

if __name__ == '__main__':


    #baseList2 = CSVDataManipulation.csvfile2strList('backup_categories_products_07062014_4_p2_v2.csv')
    #baseList3 = CSVDataManipulation.csvfile2strList('Прайс розница (требует обработки)_откорректированный_v26_edited.csv')
    #baseList4 = CSVDataManipulation.csvfile2strList('backup_categories_products_07062014_6_free.csv')
    
    #Adding products and linked data for santexnilus.by first time

    linesInFile = 1000
    dbPrefix = 'smoc'
    '''
    fixesList = [1, 2 , 3, 4, 5, 6, 7, 8, 9, 10, 4903, 4904, 4905, 4906]
    priceMode = 2
    firstProductID = 65 #First product ID on OpenCart on santexnilus.by
    #Load price lists
    baseList = CSVDataManipulation.csvfile2strList('price_with_categories_id.csv')
    assemblageList = CSVDataManipulation.strList2oc_productOC2(baseList, fixesList, priceMode, firstProductID, dbPrefix)
    CSVDataManipulation.strList2sql(assemblageList['products'], 'result_products.sql', linesInFile*0.75)
    CSVDataManipulation.strList2sql(assemblageList['product2store'], 'result_product2store.sql', linesInFile)
    CSVDataManipulation.strList2sql(assemblageList['urlAlias'], 'result_url_alias.sql', linesInFile)
    CSVDataManipulation.strList2sql(assemblageList['productDescription'], 'result_product_description.sql', linesInFile*2)
    CSVDataManipulation.strList2sql(assemblageList['product2layout'], 'result_product2layout.sql', linesInFile)
    CSVDataManipulation.strList2sql(assemblageList['product2category'], 'result_product2category.sql', linesInFile)
    '''
    #print(assemblageList)

    #Update product's price for santexnilus.by
    '''
    updateProductTuple = CSVDataManipulation.productUpdateOC2('smoc_product.csv', 'price.csv')
    updateProductSQLTuple = CSVDataManipulation.prepaireProductSQLUpdateListOC2(updateProductTuple, dbPrefix)
    CSVDataManipulation.strList2sql(updateProductSQLTuple, 'update_products.sql', linesInFile)
    '''
    #List for added images on product by SQL
    imagesList = ((1014,1030,),(1033,),(1035,),(1040,),(1047,1055,),(1068,1069,),(1095,),(1101,1103,),(1105,1109,),(1120,1128,),(1151,),(8080,),(8100,),(8110,),(8120,),(93130,),(94027,94029,),(94031,),(8130,),(8139,8140,),(93131,),(8151,8156,),(8166,),(93187,93189,),(93197,),(93243,93244,),(93141,),(8200,),(8210,),(8215,),(8218,),(93515,),(8219,8222,),(8240,),(93140,),(8251,8258,),(93514,),(93132,93133,),(8301,8302,),(8310,8311,),(8320,8327,),(8351,8355,),(93145,),(8381,8385,),(93144,),(8401,8405,),(93146,),(93454,),(8421,8423,),(93001,),(93005,),(8438,8439,),(8441,8442,),(93097,93098,),(93516,),(94112,),(94794,),(94113,94114,),(8451,8452,),(93136,),(8471,8476,),(8491,8493,),(93137,),(8501,8508,),(8510,8512,),(93142,93143,),(8550,),(93010,93013,),(8551,),(8600,8604,),(93007,93009,),(93134,),(8651,8656,),(93135,),(8691,8699,),(8701,8705,),(8721,),(8725,8726,),(8729,8731,),(8751,8756,),(8771,8772,),(8781,),(93033,),(93035,93038,),(8811,8814,),(8841,8844,),(8871,),(8875,8876,),(93944,93945,),(8900,8902,),(93946,93947,),(93571,),(93573,93578,),(93580,93585,),(93147,),(8951,8956,),(8981,8984,),(93018,),(9010,),(9020,),(9022,),(9050,),(9060,9061,),(9071,9072,),(93139,),(9081,9084,),(93002,93003,),(9101,9103,),(93030,93032,),(9121,9124,),(93138,),(9136,9139,),(93014,93015,),(9151,),(9161,9162,),(93148,),(9176,9180,),(93246,93247,),(9191,9197,),(93016,93017,),(9211,9212,),(9220,),(93004,),(93198,93199,),(9226,),(9232,),(9235,),(9238,),(94603,),(9591,),(93219,93220,))
    updateImages2ProductSQLTuple = CSVDataManipulation.prepaireAddImagesSQL2ProductOC2(imagesList, dbPrefix)
    CSVDataManipulation.strList2sql(updateImages2ProductSQLTuple, 'update_images2product.sql', linesInFile*2)

    '''
    descriptionList = CSVDataManipulation.csvfile2strList('description_ver2.csv')
    assemblageList = CSVDataManipulation.assemblageDescription(descriptionList)
    '''   
       
    #Add new product positions from price lists
    #out_oc_productList = CSVDataManipulation.strList2oc_product(baseList)
    #out_oc_product_descriptionList = CSVDataManipulation.strList2oc_product_description(baseList)
    #CSVDataManipulation.strList2sql(out_oc_productList, 'products3_6.sql')
    '''
    out_oc_productList = CSVDataManipulation.strList2oc_product(baseList4)
    out_oc_product_descriptionList = CSVDataManipulation.strList2oc_product_description(baseList4, assemblageList)
    CSVDataManipulation.strList2sql(out_oc_productList, 'products4.sql')
    CSVDataManipulation.strList2sql(out_oc_product_descriptionList, 'products4_1.sql')
    '''

    '''
    numberProduct = 5537
    baseList3Full = []
    for line in baseList3:
        product = OCProduct(line, 1, numberProduct)
        print(product.__str__())
        numberProduct = numberProduct + 1
        baseList3Full.append(product.__str__())
    out_oc_productList = CSVDataManipulation.strList2oc_product(baseList3Full)
    out_oc_product_descriptionList = CSVDataManipulation.strList2oc_product_description(baseList3Full)
    CSVDataManipulation.strList2sql(out_oc_productList + out_oc_product_descriptionList, 'products3.sql')
    '''
    
    #Converting price from EUR to USD
    '''
    out_oc_productUpdateList = CSVDataManipulation.priceUpdateUSD2EUR(425, 529, baseList)
    out_oc_productUpdateList.extend(CSVDataManipulation.priceUpdateUSD2EUR(705, 2709, baseList))
    out_oc_productUpdateList.extend(CSVDataManipulation.priceUpdateUSD2EUR(4192, 4194, baseList2))
    out_oc_productUpdateList.extend(CSVDataManipulation.priceUpdateUSD2EUR(4780, 4881, baseList2))
    CSVDataManipulation.strList2sql(out_oc_productUpdateList, 'products_update_price.sql')
    '''
    
    #Add products in category
    '''
    product2categoryList = CSVDataManipulation.addProduct2category(114, 152, 127)
    product2categoryList.extend(CSVDataManipulation.addProduct2category(153, 161, 128))
    product2categoryList.extend(CSVDataManipulation.addProduct2category(162, 172, 129))
    product2categoryList.extend(CSVDataManipulation.addProduct2category(173, 193, 130))
    product2categoryList.extend(CSVDataManipulation.addProduct2category(194, 228, 118))
    product2categoryList.extend(CSVDataManipulation.addProduct2category(229, 255, 123))
    product2categoryList.extend(CSVDataManipulation.addProduct2category(256, 257, 128))
    CSVDataManipulation.strList2sql(product2categoryList, 'products2category_2.sql')
    '''
    
    
    #Add products in category (alternative)
    '''
    product2categoryList2 = CSVDataManipulation.csvfile2strList('Прайс откорректированный_id_cat_id_free.csv')
    sqlList = CSVDataManipulation.addProductList2category(product2categoryList2)
    CSVDataManipulation.strList2sql(sqlList, 'products2category_3.sql')
    
    product2categoryList3 = CSVDataManipulation.csvfile2strList('Прайс откорректированный_id_cat_id_free_2.csv')
    sqlList = CSVDataManipulation.addProductList2category(product2categoryList3)
    CSVDataManipulation.strList2sql(sqlList, 'products2category_4.sql')
    
    product2categoryList3 = CSVDataManipulation.csvfile2strList('Прайс откорректированный pdf_id_cat_id.csv')
    sqlList = CSVDataManipulation.addProductList2category(product2categoryList3)
    CSVDataManipulation.strList2sql(sqlList, 'products2category_5.sql')
    '''
    
    #Add products to story
    '''
    product2story = CSVDataManipulation.addProduct2store(2923, 4129)
    product2story.extend(CSVDataManipulation.addProduct2store(4131, 4559))
    product2story.extend(CSVDataManipulation.addProduct2store(4561, 5445))
    product2story.extend(CSVDataManipulation.addProduct2store(5537, 8407))
    CSVDataManipulation.strList2sql(product2story, 'products2store.sql')
    product2story2 = CSVDataManipulation.addProduct2store(8757, 10116)
    CSVDataManipulation.strList2sql(product2story2, 'products2store_2.sql')
    '''
    
    