'''
Created on 16 июня 2014 г.

@author: Egor N. Kostyuk
'''
import codecs
import math

def clearS(inStr):
    if isinstance(inStr, str):
        tmpStr = inStr

        #remove all spaces
        if isinstance(inStr, str):
            tmpStr = tmpStr.replace(' ', '')

        #remove unused zero after point (tmpChars[1] - numbers after point) if point exists
        tmpChars = list(tmpStr.split('.'))
        if len(tmpChars) == 2:
            i = len(tmpChars[1])
            afterPoint = list(tmpChars[1])
            while ( afterPoint[:-1] != '0' ):
                afterPoint = afterPoint[0:-1]
                i = i-1
                if i == 0:
                    break
            if len(afterPoint) != 0:
                tmpChars = tmpChars[0] + '.'
                tmpStr = tmpChars.join(afterPoint)
            else:
                tmpStr = tmpChars[0]

        return tmpStr
    else:
        return inStr



def inc(i , countInc = 1):
    counter = 0
    for counter in range(0, countInc):
        i = i + 1
    return i

def insQ(inStr):
    return str('\'' + inStr + '\'')

def boolStr2Int(inStr):
    if inStr == 'yes':
        result = '1'
    elif inStr =='no':
        result = '0'
    elif inStr == 'true':
        result = '1'
    elif inStr == 'false':
        result = '0'
    else:
        result = inStr
    return result

def notZero(inStr):
    if inStr == '':
        return '0'
    else:
        return inStr

#Conveting double quote symbol on string to html code
def convQ(inStr):
    tmpStr = []
    for symbol in inStr:
        if symbol == '"':
            tmpStr.append('&quot;')
        else:
            tmpStr.append(symbol)
    return "".join(tmpStr)
    
class OCProduct():
    def __init__(self, strProductList, priceMode = 0, product_id = 0):
        self.strList2Product(strProductList, priceMode, product_id)
        
    @classmethod
    def strList2Product(self, strProductList, priceMode, product_id):
        i = 0
        
        if priceMode == 0:     
            setattr(self, 'product_id', strProductList[i])
            i = inc(i)
            setattr(self, 'name', strProductList[i])
            i = inc(i)
            setattr(self, 'categories', strProductList[i])
            i = inc(i)
            setattr(self, 'sku', strProductList[i])
            i = inc(i)
            setattr(self, 'upc', strProductList[i])
            i = inc(i)
            setattr(self, 'ean', strProductList[i])
            i = inc(i)
            setattr(self, 'jan', strProductList[i])
            i = inc(i)
            setattr(self, 'isbn', strProductList[i])
            i = inc(i)
            setattr(self, 'mpn', strProductList[i])
            i = inc(i)
            setattr(self, 'location', strProductList[i])
            i = inc(i)
            setattr(self, 'quantity', strProductList[i])
            i = inc(i)
            setattr(self, 'model', strProductList[i])
            i = inc(i)
            setattr(self, 'manufacturer', strProductList[i])
            i = inc(i)
            setattr(self, 'image_name', strProductList[i])
            i = inc(i)
            setattr(self, 'requires_shipping', strProductList[i])
            i = inc(i)
            if strProductList[i] != '' or strProductList[i] != ' ':
                setattr(self, 'price', strProductList[i])
            else:
                setattr(self, 'price', '0.0000')
            i = inc(i)
            setattr(self, 'points', strProductList[i])
            i = inc(i)
            setattr(self, 'date_added', strProductList[i])
            i = inc(i)
            setattr(self, 'date_modified', strProductList[i])
            i = inc(i)
            setattr(self, 'date_available', strProductList[i])
            i = inc(i)
            setattr(self, 'weight', strProductList[i])
            i = inc(i)
            setattr(self, 'unit', strProductList[i])
            i = inc(i)
            setattr(self, 'length', strProductList[i])
            i = inc(i)
            setattr(self, 'width', strProductList[i])
            i = inc(i)
            setattr(self, 'height', strProductList[i])
            i = inc(i)
            setattr(self, 'length_unit', strProductList[i])
            i = inc(i)
            setattr(self, 'status_enabled', strProductList[i])
            i = inc(i)
            setattr(self, 'tax_class_id', strProductList[i])
            i = inc(i)
            setattr(self, 'viewed', strProductList[i])
            i = inc(i)
            setattr(self, 'language_id', strProductList[i])
            i = inc(i)
            setattr(self, 'seo_keyword', strProductList[i])
            i = inc(i)
            setattr(self, 'description', strProductList[i])
            i = inc(i)
            setattr(self, 'meta_description', strProductList[i])
            i = inc(i)
            setattr(self, 'meta_keywords', strProductList[i])
            i = inc(i)
            setattr(self, 'stock_status_id', strProductList[i])
            i = inc(i)
            setattr(self, 'store_ids', strProductList[i])
            i = inc(i)
            setattr(self, 'layout', strProductList[i])
            i = inc(i)
            setattr(self, 'related_ids', strProductList[i])
            i = inc(i)
            setattr(self, 'tags', strProductList[i])
            i = inc(i)
            setattr(self, 'sort_order', strProductList[i])
            i = inc(i)
            setattr(self, 'subtract', strProductList[i])
            i = inc(i)
            setattr(self, 'minimum', strProductList[i])
            
            #set default value
            setattr(self, 'weight_class_id', '0')
            setattr(self, 'length_class_id', '0')
            
        #Especial mode for specific price for aqugold.by
        if priceMode == 1:
            setattr(self, 'manufacturer_name', strProductList[i])
            #print('manufacturer_name=' + self.manufacturer_name)
            i = inc(i)
            setattr(self, 'sku', strProductList[i])
            #print('sku=' + self.sku)
            i = inc(i)
            productName = strProductList[i] + ' ' + strProductList[i+1]
            setattr(self, 'name', productName)
            #print('name=' + self.name)
            i = inc(i, 2)
            descriptionList = [strProductList[i + n] for n in range(0, 4) ]
            productDescription = ''
            for item in descriptionList:
                if item != '':
                    productDescription = productDescription + item + ' '
                else:
                    next
            productDescription = ''.join(list(productDescription)[:-1])
            setattr(self, 'description', productDescription)
            #print('description=' + self.description)
            i = inc(i, 5)
            setattr(self, 'price', str(strProductList[i]).replace(',', '.'))
            #print('price=' + self.price)
            setattr(self, 'product_id', str(product_id))

        #Especial mode for specific price for santexnikus.by
        if priceMode == 2:
            setattr(self, 'product_id', str(product_id))
            setattr(self, 'sku', strProductList[0])
            setattr(self, 'name', strProductList[2])
            setattr(self, 'price', str(strProductList[4]))
            setattr(self, 'description', '')
            
            #set default value
            setattr(self, 'categories', '')
            setattr(self, 'upc', '')
            setattr(self, 'ean', '')
            setattr(self, 'jan', '')
            setattr(self, 'isbn', '')
            setattr(self, 'mpn', '')
            setattr(self, 'location', '')
            setattr(self, 'quantity', '1')
            setattr(self, 'model', self.name)
            setattr(self, 'manufacturer', '')
            setattr(self, 'image_name', '')
            setattr(self, 'requires_shipping', '1')
            setattr(self, 'points', '0')
            setattr(self, 'date_added', '2009-02-03')
            setattr(self, 'date_modified', '2009-02-03')
            setattr(self, 'date_available', '2009-02-03')
            setattr(self, 'weight', '')
            setattr(self, 'unit', '0')
            setattr(self, 'length', '')
            setattr(self, 'width', '')
            setattr(self, 'height', '')
            setattr(self, 'length_unit', '0')
            setattr(self, 'status_enabled', '1')
            setattr(self, 'tax_class_id', '0')
            setattr(self, 'viewed', '0')
            setattr(self, 'language_id', '0')
            setattr(self, 'seo_keyword', '')
            setattr(self, 'meta_description', '')
            setattr(self, 'meta_keywords', '')
            setattr(self, 'stock_status_id', '0')
            setattr(self, 'store_ids', '0')
            setattr(self, 'layout', '0')
            setattr(self, 'related_ids', '0')
            setattr(self, 'tags', '')
            setattr(self, 'sort_order', '0')
            setattr(self, 'subtract', '0')
            setattr(self, 'minimum', '1')
            
            #set default value
            setattr(self, 'weight_class_id', '0')
            setattr(self, 'length_class_id', '0')


    
    def __str__(self):
        tmpList = (self.product_id, self.name, self.categories, self.sku, self.upc, self.ean,
                   self.jan, self.isbn, self.mpn, self.location, self.quantity, self.model,
                   self.manufacturer, self.image_name, self.requires_shipping, self.price,
                   self.points, self.date_added, self.date_modified, self.date_available, 
                   self.weight, self.unit, self.length, self.width, self.height, self.length_unit,
                   self.status_enabled, self.tax_class_id, self.viewed, self.language_id,
                   self.seo_keyword, self.description, self.meta_description, self.meta_keywords,
                   self.stock_status_id, self.store_ids, self.layout, self.related_ids, self.tags,
                   self.sort_order, self.subtract, self.minimum)
        return list(tmpList)
    
    @classmethod
    def getPrice(cls):
        #remove spaces on price
        resultPrice = cls.price.replace(' ', '')
        if resultPrice == '':
            return '0.0000'
        else:
            return resultPrice
    
    @classmethod
    def getWeight(cls):
        if cls.weight == '':
            return '0.00000000'
        else:
            return cls.weight
        
    @classmethod
    def getWidth(cls):
        if cls.width == '':
            return '0.00000000'
        else:
            return cls.width
        
    @classmethod
    def getLength(cls):
        if cls.length == '':
            return '0.00000000'
        else:
            return cls.length
        
    @classmethod
    def getHeight(cls):
        if cls.height == '':
            return '0.00000000'
        else:
            return cls.height
    
        
class CSVDataManipulation():
    '''
    class for working with csv files
    '''
    
    @staticmethod
    def csvfile2strList(fileName, sep=';', encoding='utf-8'):
        
        '''
        def strimList(inList):
            outList = []
            for curStr in inList:
                tmpStr = curStr.lstrip().rstrip()
                outList.append(tmpStr)
            return outList
            
        '''

        if encoding=='':
            f = open(fileName)
        else:
            f = codecs.open(fileName, 'r', encoding)
        tmpList = []
        for line in f:
            lineList = list(line)
            lineList[:] = lineList[:-1]
            tmpStr = "".join(lineList)
            #tmpStr = tmpStr.replace(" ", "")
            lineList = list(tmpStr.split(sep))
            print(tmpStr)
            tmpList.append(lineList)
        #tmpList[:] = tmpList[1:]
        return tmpList
    
    @staticmethod
    #linesInFile - count of line in every file. If linesInFile=0 then all lines on one file.
    def strList2sql(strList, fileFullName, linesInFile=0):
        if linesInFile == 0:
            linesInFile = len(strList)
        countLines = len(strList)
        lastIndex = countLines - 1
        countFiles = math.ceil(countLines / linesInFile)
        l = 1 #counter for lines
        f = 1 #counter for files
        fileName, fileExtension = fileFullName.split('.')
        headerSQL = strList[0] + '\n'
        while f <= countFiles:
            fileFullName = fileName + '_' + str(f) + '.' + fileExtension
            file = codecs.open(fileFullName, 'w', 'utf-8')
            #write to file sql header instruction
            file.write(''.join(headerSQL))
            while l <= lastIndex:
                if ( l >= f*linesInFile ) or ( l == lastIndex ) :
                    #replace last "," to ";"
                    line = strList[l][:-1] + ';'
                else:
                    line = strList[l] + '\n'
                file.write(''.join(line))
                #end of file - need exit for write next file
                if l >= f*linesInFile:
                    file.close()
                    l = inc(l)
                    break
                l = inc(l)
            file.close()
            f = inc(f)
       
    @staticmethod
    def strList2oc_product(strList):
        oc_productList = ['INSERT INTO `oc_product` (`product_id`, `model`, `sku`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `quantity`, `stock_status_id`, `image`, `manufacturer_id`, `shipping`, `price`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `subtract`, `minimum`, `sort_order`, `status`, `date_added`, `date_modified`, `viewed`) VALUES']
        i = 0
        for line in strList:
            i = inc(i)
            product = OCProduct(line)
            print(product.__str__())
            sqlList = [str(product.product_id), insQ(product.model), insQ(product.sku), insQ(product.upc),
                       insQ(product.ean), insQ(product.jan), insQ(product.isbn), insQ(product.mpn), insQ(product.location),
                       str(product.quantity), str(product.stock_status_id), insQ(product.image_name), notZero(product.manufacturer),
                       boolStr2Int(product.requires_shipping),
                       insQ(product.getPrice()),
                       notZero(product.points),
                       notZero(product.tax_class_id),
                       insQ(product.date_available),
                       insQ(product.getWeight()),
                       str(product.weight_class_id),
                       insQ(product.getLength()),
                       insQ(product.getWidth()),
                       insQ(product.getHeight()), 
                       str(product.length_class_id), boolStr2Int(product.subtract),
                       insQ(product.minimum),
                       insQ(product.sort_order),
                       boolStr2Int(product.status_enabled),
                       insQ(product.date_added), insQ(product.date_modified), notZero(product.viewed)]
            tmpStr = '(' + ','.join(sqlList) + ')'
            if i != len(strList):
                tmpStr = tmpStr + ','
            else:
                tmpStr = tmpStr + ';'
            oc_productList.append(tmpStr)
        return oc_productList

    '''
    method for OpenCart2 (tested on 2.1.0.1 ocStore)
    '''
    @staticmethod
    def strList2oc_productOC2(strList, fixesList=[], priceMode = 0, firstProductID = 0, dbPrefix = 'oc', shopID = '0', firstProductLinkID = 1133 ):
        oc_productList = ['INSERT INTO `'+ dbPrefix +'_product` (`product_id`, `model`, `sku`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `quantity`, `stock_status_id`, `image`, `manufacturer_id`, `shipping`, `price`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `subtract`, `minimum`, `sort_order`, `status`, `viewed`, `date_added`, `date_modified`) VALUES']
        oc_product_to_storeList = ['INSERT INTO `'+ dbPrefix +'_product_to_store` (`product_id`, `store_id`) VALUES']
        oc_url_alias = ['INSERT INTO `'+ dbPrefix +'_url_alias` (`url_alias_id`, `query`, `keyword`) VALUES']
        oc_product_description = ['INSERT INTO `'+ dbPrefix +'_product_description` (`product_id`, `language_id`, `name`, `description`, `tag`, `meta_title`, `meta_h1`, `meta_description`, `meta_keyword`) VALUES']
        oc_product_to_layout = ['INSERT INTO `'+ dbPrefix +'_product_to_layout` (`product_id`, `store_id`, `layout_id`) VALUES']
        oc_product_to_category= ['INSERT INTO `'+ dbPrefix +'_product_to_category` (`product_id`, `category_id`, `main_category`) VALUES']
        i = 1
        productID = firstProductID
        productLinkID = firstProductLinkID
        curCategoryID = ''
        for line in strList:
            #print(line)

            #Skipping specified lines
            if i in fixesList:
                i = inc(i)
                continue

            #print(line)

            #Skipping if this not a product
            if line[0] == '':
                #Set current category if this not a product's line
                if line[1] != "":
                    curCategoryID = line[1]
                else:
                    curCategoryID = ''
                continue

            product = OCProduct(line, priceMode, productID)
            print(i)
            i = inc(i)
            productID = inc(productID)
            print(product.__str__())
            sqlList = [str(product.product_id), insQ(convQ(product.model)), insQ(product.sku), insQ(product.upc),
                       insQ(product.ean), insQ(product.jan), insQ(product.isbn), insQ(product.mpn), insQ(product.location),
                       str(product.quantity), str(product.stock_status_id), insQ(product.image_name), notZero(product.manufacturer),
                       boolStr2Int(product.requires_shipping),
                       insQ(product.getPrice()),
                       notZero(product.points),
                       notZero(product.tax_class_id),
                       insQ(product.date_available),
                       insQ(product.getWeight()),
                       str(product.weight_class_id),
                       insQ(product.getLength()),
                       insQ(product.getWidth()),
                       insQ(product.getHeight()),
                       str(product.length_class_id), boolStr2Int(product.subtract),
                       insQ(product.minimum),
                       insQ(product.sort_order),
                       boolStr2Int(product.status_enabled),
                       notZero(product.viewed),
                       insQ(product.date_added), insQ(product.date_modified)]
            tmpStr = '(' + ','.join(sqlList) + '),'
            oc_productList.append(tmpStr)
            tmpStr2 = '(' + str(product.product_id) + ',' + shopID + '),'
            oc_product_to_storeList.append(tmpStr2)
            tmpStr3 = '(' + str(productLinkID) + ',\'product_id=' + str(product.product_id) + '\',\'\'),'
            oc_url_alias.append(tmpStr3)
            productLinkID = inc(productLinkID)
            tmpStr4 = '(' + str(product.product_id) + ',1,' + insQ(convQ(product.name)) + ',\'\',\'\',\'\',\'\',\'\',\'\'),'
            oc_product_description.append(tmpStr4)
            tmpStr4 = '(' + str(product.product_id) + ',2,' + insQ(convQ(product.name)) + ',\'\',\'\',\'\',\'\',\'\',\'\'),'
            oc_product_description.append(tmpStr4)
            tmpStr5 = '(' + str(product.product_id) + ',' + shopID + ',0),'
            oc_product_to_layout.append(tmpStr5)
            if curCategoryID != '':
                tmpStr6 = '(' + str(product.product_id) + ',' + curCategoryID + ',1),'
                oc_product_to_category.append(tmpStr6)
        resultDic = {'products': oc_productList, 'product2store': oc_product_to_storeList, 'urlAlias': oc_url_alias, 'productDescription': oc_product_description, 'product2layout': oc_product_to_layout, 'product2category': oc_product_to_category}
        return resultDic
    
    @staticmethod
    def strList2oc_product_description(strList, descriptionList = []):
        oc_product_descriptionList = ['INSERT INTO `oc_product_description` (`product_id`, `language_id`, `name`, `description`, `meta_description`, `meta_keyword`, `tag`) VALUES']
        i = 0
        for line in strList:
            i = inc(i)
            product = OCProduct(line)
            description = insQ(product.description)
            if descriptionList != []:
                if int(product.product_id) != int(descriptionList[i-1][0]):
                    print('ERORR! product.product_id is not equivalent to product_id from descriptionList('
                          + str(product.product_id) + ' != ' + str(descriptionList[i-1][0]) + ')')
                else:
                    print(descriptionList[i-1])
                    description = descriptionList[i-1][1]
                    print(str(description))
                #description = descriptionList[i][]
            sqlList = [str(product.product_id), str(product.language_id), insQ(product.name), insQ(description),
                       insQ(product.meta_description), insQ(product.meta_keywords), insQ(product.tags)]
            tmpStr = '(' + ','.join(sqlList) + ')'
            if i != len(strList):
                tmpStr = tmpStr + ','
            else:
                tmpStr = tmpStr + ';'
            oc_product_descriptionList.append(tmpStr)
        return oc_product_descriptionList
    
    @staticmethod
    def priceUpdateUSD2EUR(product_idFrom, product_idTo, strList, conEUR2USD=1.36):
        sqlList = []
        for line in strList:
            product = OCProduct(line)
            if int(product.product_id) in range(product_idFrom, product_idTo + 1):
                try:
                    newPrice = round(float(product.price)*conEUR2USD, 2)
                    tmpStr = 'UPDATE `oc_product` SET `price` = ' + str(newPrice)+' WHERE `product_id` = ' + str(product.product_id) + ';'
                    print(tmpStr)
                    sqlList.append(tmpStr)
                except:
                    print('(id=' + product.product_id + ')Conv Error!')
        return sqlList
    
    @staticmethod
    def addProduct2category(product_idFrom, product_idTo, category_id):
        sqlList = ['INSERT INTO `oc_product_to_category` (`product_id`, `category_id`) VALUES']
        i = 0
        lineEnd = '),'
        for i in range(product_idFrom, product_idTo + 1):
            if i == product_idTo:
                lineEnd = ');'
            tmpStr = '('+ str(i) + ', ' + str(category_id) + lineEnd
            sqlList.append(tmpStr)
        return sqlList
    
    @staticmethod
    def addProductList2category(product2categoryList):
        introStr = 'INSERT INTO `oc_product_to_category` (`product_id`, `category_id`) VALUES'
        lenList = len(product2categoryList)
        countBlocks = (lenList // 500) + 1
        isLastItem = False
        i = 0
        for i in range(0 , countBlocks):
            k = 0
            lineEnd = '),'
            if k == 0 and i == 0:
                sqlList = [introStr]
            else:
                sqlList.append(introStr)
            for k in range(0 , 500):
                curIndex = i*500 + k
                if curIndex == (lenList-1):
                    isLastItem = True
                if k == 499 or isLastItem:
                    lineEnd = ');'
                sqlList.append('(' + product2categoryList[curIndex][0] + ', ' + product2categoryList[curIndex][1] + lineEnd)
                if isLastItem:
                    print(sqlList)
                    return sqlList
                k = k + 1
            i = i + 1
            
    @staticmethod
    def addProduct2store (product_idFrom, product_idTo, store_id = '0'):
        introStr = 'INSERT INTO `oc_product_to_store` (`product_id`, `store_id`) VALUES'
        sqlList = [introStr]
        i = product_idFrom
        lineEnd = '),'
        for i in range(product_idFrom, product_idTo + 1):
            if i == product_idTo:
                lineEnd = ');'
            sqlList.append('(' + str(i) + ', ' + store_id + lineEnd)
            i = i + 1
        return sqlList
    
    @staticmethod
    def assemblageDescription(strList):
        assemblageList = []
        for line in strList:
            i = 1
            tmpStr = ''
            for i in range(1, len(line)):
                if line[i] != '':
                    tmpStr = tmpStr + line[i]
                    if i < (len(line)-1):
                        tmpStr = tmpStr + ' '
                i = i + 1
            assemblageList.append([int(line[0]), tmpStr])
        return assemblageList

    '''
    method for OpenCart2 (tested on 2.1.0.1 ocStore)
    '''
    @staticmethod
    def productUpdateOC2 (oldCSVDump, newCSVPrice='', updateSQL='update_products.sql'):
        oldProductList = CSVDataManipulation.csvfile2strList(oldCSVDump, '~')

        #create dict with current prices on site
        oldPriceDict = {}
        for oldProduct in oldProductList:
            #oldProduct[2] is ID of product, oldProduct[14] is current price of product
            oldPriceDict[oldProduct[2]] = oldProduct[14]

        #calculate dict with prices for update
        newPriceList = []
        newProductList = CSVDataManipulation.csvfile2strList(newCSVPrice, encoding='')
        for newProduct in newProductList:
            #newProduct[0] is vendor code of the product, newProduct[1] is number of the product on pricelist, newProduct[4] is new price of the product
            if (len(newProduct) >= 5 and newProduct[1] != '' and newProduct[4] != ''):
                newPrice = clearS(newProduct[4])
                oldPrice = clearS(oldPriceDict.get(newProduct[0]))
                if (oldPrice != newPrice) and oldPrice is not None:
                    print('Обновление товара {0}({1}) с {2} BYR на {3} BYR'.format(newProduct[0], newProduct[2] , oldPrice, newPrice))
                    newPriceList.append((newProduct[0], newPrice,))

        return tuple(newPriceList)

    '''
    method for OpenCart2 (tested on 2.1.0.1 ocStore)
    '''
    @staticmethod
    def prepaireProductSQLUpdateListOC2(updateList, dbPrefix):
        templatSQL = "UPDATE `{0}_product` SET `price`={1} WHERE `sku`={2};"
        updateResultList = []
        for product in updateList:
            sqlLine = templatSQL.format(dbPrefix, product[1], product[0])
            updateResultList.append(sqlLine)
        return updateResultList

    '''
    method for OpenCart2 (tested on 2.1.0.1 ocStore)
    imagesList contain list with 1 item or 2 items (this's range of SKU images)
    '''
    @staticmethod
    def prepaireAddImagesSQL2ProductOC2(imagesList, dbPrefix):
        templatSQL = "UPDATE `{0}_product` SET `image`='catalog/products/{1}.jpg' WHERE `sku`={1};"
        updateResultList = []
        rangeSKU = []
        #generate list with SKU images name for update
        for itemList in imagesList:
            #if itemList has 2 items then is range else has 1 item
            if len(itemList) == 2:
                rangeSKU.extend([imageSKU for imageSKU in range(itemList[0], itemList[1]+1)])
            else:
                rangeSKU.append(itemList[0])

        #generate sql list for update
        for sku in rangeSKU:
            updateResultList.append(templatSQL.format(dbPrefix, sku))
        return tuple(updateResultList)